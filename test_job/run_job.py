#!/usr/bin/env python3
# pylint: disable=too-many-locals, too-few-public-methods, too-many-statements
"""
Script that runs a job to test the system
"""
import argparse
import json
import time
from pathlib import Path
import hashlib

import requests
import yaml

PARSER = argparse.ArgumentParser()
PARSER.add_argument('run_params_file', help='The path of the file with the run params')
PARSER.add_argument('-v', '--verbose', help='Make output verbose', action='store_true')
PARSER.add_argument('-n', '--dry-run', help='Do not actually send progress to the delayed jobs server',
                    action='store_true')
ARGS = PARSER.parse_args()


class DelayedJobsServerConnection:
    """
    Class that encapsulates functions to communicate the progress of the job to the server
    """

    def __init__(self, run_params_file_path, dry_run=False):
        """
        Constructor of the class, allows to create a connection object with the parameters provided
        :param run_params_file_path: path to the run params file
        :param dry_run: if true, do not send anything to the server
        """

        run_params = yaml.load(open(run_params_file_path, 'r'), Loader=yaml.FullLoader)
        status_update_config = run_params.get('status_update_endpoint')
        self.job_status_update_url = status_update_config.get('url')
        self.job_status_update_method = status_update_config.get('method')

        custom_statistics_config = run_params.get('custom_statistics_endpoint')
        self.custom_statistics_url = custom_statistics_config.get('url')
        self.custom_statistics_update_method = custom_statistics_config.get('method')

        self.job_token = run_params.get('job_token')
        self.dry_run = dry_run

    def update_job_progress(self, progress_percentage, status_log_msg):
        """
        Updates the job's progress percentage with the one given as parameter, sends also a msg with status_log_msg
        if you want
        :param progress_percentage: current progress percentage
        :param status_log_msg: if you want me to print a verbose output
        :return:
        """
        print('--------------------------------------------------------------------------------------')
        print(f'Setting progress percentage to {progress_percentage}')
        print(f'Adding msg to status log: {status_log_msg}')

        url = self.job_status_update_url
        job_token = self.job_token
        headers = {
            'X-Job-Key': job_token
        }
        payload = {
            'progress': progress_percentage,
            'status_log': status_log_msg,
        }
        print(f'url: {url}')
        print(f'job_token: {job_token}')
        print('Headers:')
        print(headers)
        print('payload:')
        print(payload)

        print('--------------------------------------------------------------------------------------')
        if self.dry_run:
            print('Not actually sending message to the server (Dry run)')
        else:
            request = requests.patch(url, payload, headers=headers)
            print('Progress Sent to server. Response status code: ', request.status_code)
            print('Response text: ', request.text)

    def send_job_statistics(self, stats_dict):
        """
        Sends to the Delajed Jobs Server statistics of this job.
        :param stats_dict: dict with the stats for the job to send. See the delayed jobs documentation to know
        which is are the properties you have to send
        """
        print('--------------------------------------------------------------------------------------')
        print(f'Sending statistics. stats_obj: {stats_dict}')

        url = self.custom_statistics_url
        job_token = self.job_token
        headers = {
            'X-Job-Key': job_token
        }

        print(f'url: {url}')
        print(f'job_token: {job_token}')
        print('Headers:')
        print(headers)
        print('payload:')
        print(stats_dict)

        if self.dry_run:
            print('Not actually sending statistics to the server (Dry run)')
        else:
            request = requests.post(url, stats_dict, headers=headers)
            print('Statistics Sent to server. Response status code: ', request.status_code)
            print('Response text: ', request.text)


def run():
    """
    Runs the job
    """

    run_params = yaml.load(open(ARGS.run_params_file, 'r'), Loader=yaml.FullLoader)
    job_params = run_params.get('job_params')
    duration = int(job_params.get('seconds'))
    instruction = job_params.get('instruction')
    inputs = run_params.get('inputs')
    output_path = run_params.get('output_dir')
    dry_run = run_params.get('dry_run', ARGS.dry_run)

    server_connection = DelayedJobsServerConnection(run_params_file_path=ARGS.run_params_file, dry_run=dry_run)

    print('JOB PARAMS:')
    print(json.dumps(job_params, indent=4))
    print('---------------------------------------------------------------------------------------')
    print('Reading Inputs')
    print('---------------------------------------------------------------------------------------')

    for input_key, input_path in inputs.items():
        print(f'Reading input {input_key}')
        input_full_path = Path(input_path).resolve()
        print(f'Full path is {input_full_path}')
        with open(input_full_path, 'rb') as input_file:
            bytes_content = input_file.read()
            print('SHA256: ')
            print(hashlib.sha256(bytes_content).hexdigest())

        print('---')

    print('---------------------------------------------------------------------------------------')
    print('Consuming an API')
    print('---------------------------------------------------------------------------------------')

    api_url = job_params.get('api_url')
    print(f'I am going to consume {api_url}')
    request = requests.get(api_url)
    status_code = request.status_code

    if status_code == 200:

        print('response: ')
        print(request.json())

    else:

        print(f'The API was not reachable! Status code was: {status_code}')

    print('---------------------------------------------------------------------------------------')
    print('Printing messages and sending progress')
    print('---------------------------------------------------------------------------------------')

    for i in range(1, duration + 1):
        time.sleep(1)
        progress = int((i / duration) * 100)
        print(f'progress: {progress}')

        messages = ['Running jobs, all day long, running jobs while I sing this song...',
                    'Illusions, Mr. Anderson. Vagaries of perception. The temporary constructs of a feeble human '
                    'intellect trying desperately to justify an existence that is without meaning or purpose', None]

        server_connection.update_job_progress(progress, messages[i % 3])

    print('---------------------------------------------------------------------------------------')
    print('Printing output files')
    print('---------------------------------------------------------------------------------------')

    output_dir_path = Path(output_path).resolve()
    print(f'I am going to write the output in {output_dir_path}')

    if instruction == 'FAIL':
        raise Exception('I must fail')

    for i in range(1, 5):
        with open(Path.joinpath(output_dir_path, f'output_{i}.txt'), 'w') as output_file:
            output_file.write(f'This is output file {i}')

    print('---------------------------------------------------------------------------------------')
    print('Send statistics')
    print('---------------------------------------------------------------------------------------')

    stats_dict = {
        'duration': duration
    }
    server_connection.send_job_statistics(stats_dict)


if __name__ == "__main__":
    run()
