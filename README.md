This project defines a sample job that can be run in the Delayed Jobs system using a container. It tests the basic 
actions that a job performs:

- Receives some parameters and some input files.
- Consumes an API.
- Prints some mock progress messages to stdout. 
- Produces some output files. 

The job is expected to be run like this by the Delayed Jobs system:

```bash
/app/run_job.sh run_params.yml
```

This means that in the container, the path '/app/run_job.sh' must provide a script that receives a run parameters file
in yaml and executes the job with it.

Here is an example of the run parameters file for this test job:

```yaml
job_id: someJobID
job_token: someToken
output_dir: 'test/output_1'
inputs:
  input1: 'test/params/input_file1.txt'
  input2: 'test/params/input_file2.txt'
job_params:
  instruction: 'RUN_NORMALLY'
  seconds: 5
  api_url: 'https://www.ebi.ac.uk/chembl/api/data/similarity/CN1C(=O)C=C(c2cccc(Cl)c2)c3cc(ccc13)[C@@](N)(c4ccc(Cl)cc4)c5cncn5C/80.json'
status_update_endpoint:
  method: PATCH
  url: http://delayed-jobs-server/chembl/interface_api/delayed_jobs/status/someJobID
``` 

What goes inside 'job_params' is free for the job developer decide. The fields related to the inputs and 
outputs, the 'inputs' and 'output_dir' fields, tell the job where to get the
inputs from and where to write the output. 

For example, a parameters file for a job doing a similarity search could be:

```yaml
job_id: someJobID
job_token: someToken
output_dir: 'test/output_1'
inputs:
  input1: 'test/params/input_file1.txt'
  input2: 'test/params/input_file2.txt'
job_params:
  query: 'CN1C(=O)C=C(c2cccc(Cl)c2)c3cc(ccc13)[C@@](N)(c4ccc(Cl)cc4)c5cncn5C'
  threshold: 80
status_update_endpoint:
  method: PATCH
  url: http://delayed-jobs-server/chembl/interface_api/delayed_jobs/status/someJobID
```

To test the job in the LSF system, create a run_params.yml file, some input files, and an output directory. 
Then run the following command:

```bash
bsub -Is "singularity exec docker://dockerhub.ebi.ac.uk/chembl/chembl/delayed-jobs/test-job:latest /app/run_job.sh run_params.yml"
```

The status_update_endpoint defines the endpoint and method to use in order to send a progress update to the server. To 
see how it works, see the DelayedJobsServerConnection class in the sample script(test_job/run_job.py) in this project.

