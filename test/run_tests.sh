#!/usr/bin/env bash

set -x

# ----------------------------------------------------------------------------------------------------------------------
# Simulate job that runs normally
# ----------------------------------------------------------------------------------------------------------------------

rm -rf $PWD/test/output_1 || true
mkdir -p $PWD/test/output_1
$PWD/test_job/run_job.py $PWD/test/params/run_params_example.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/test/output_1

# ----------------------------------------------------------------------------------------------------------------------
# Simulate job that fails
# ----------------------------------------------------------------------------------------------------------------------

rm -rf $PWD/test/output_2 || true
mkdir -p $PWD/test/output_2
$PWD/test_job/run_job.py $PWD/test/params/run_params_example2.yml -n
EXIT_CODE_2=$?

if [[ $EXIT_CODE_2 -eq 1 ]]; then
    echo "The job failed, as expected"
else
    echo "The job didn't fail! Please check"
    exit 1
fi

ls -lah $PWD/test/output_2
